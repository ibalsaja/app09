package lana.ahmad.myapplication.app09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {

        companion object{
        val DB_Name = "music"
        val DB_Ver = 1
    }

        override fun onCreate(db: SQLiteDatabase?) {
            val tMusik = "create table musik(id_music int primary key,id_cover int,music_title text)"
            val insT = " insert into musik values('0x7f0c0000','0x7f060061','ledzeppelin'),('0x7f0c0001','0x7f060062','oasis'),('0x7f0c0002','0x7f060063','queen')"
            db?.execSQL(tMusik)
            db?.execSQL(insT)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

        }
}
