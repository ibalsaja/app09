package lana.ahmad.myapplication.app09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {


    var posLaguSkr = 0
    var posCoverSkr = 0
    var posJudulSkr = ""
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController
    lateinit var db: SQLiteDatabase
    lateinit var adapter : ListAdapter
    var idMusik : Int = 0x7f0c0000
    var idCover : Int = 0x7f060061
    var idJudul : String = "ledzeppelin"
    var pMs : Int = idMusik
    var pCs : Int = idCover
    var pJs : String = idJudul
    var max : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        seekBar.max = 100
        seekBar.progress = 0
        seekBar.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        db=DBOpenHelper(this).writableDatabase
        lvMusik.setOnItemClickListener(itemClicked)
    }

    val itemClicked= AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idMusik = c.getInt(c.getColumnIndex("_id"))
        txJudulLagu.setText(c.getString(c.getColumnIndex("judul")))
        idMusik = c.getInt(c.getColumnIndex("_id"))
        idCover = c.getInt(c.getColumnIndex("cover"))
        idJudul = c.getString(c.getColumnIndex("judul"))
        Toast.makeText(this,"Perubahan Telah Disimpan $idMusik, $idCover, $idJudul", Toast.LENGTH_SHORT).show()
        audioplaygo(idMusik,idCover,idJudul)
    }

    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioplaygo(a:Int,b:Int,c:String){
        mediaPlayer.stop()
        mediaPlayer = MediaPlayer.create(this,a)
        seekBar.max = mediaPlayer.duration
        txJudulLagu.setText(idJudul)
        txMaxTime.setText(milliSecondToString(seekBar.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        imV.setImageResource(b)
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread,50)
    }

    override fun onStart() {
        super.onStart()
        showdata()
        pl()
    }

    fun showdata(){
        var sql = "select id_music as _id, id_cover as cover,music_title as judul from musik"
        max = "select count(id_music) as i from musik"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(this,R.layout.item_data_musik,c, arrayOf("_id","cover","judul"), intArrayOf(R.id.txIdMusik,R.id.txIdCover,R.id.txMusikTitle),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lvMusik.adapter = adapter
    }

    fun pl(){
        posLaguSkr = pMs
        posCoverSkr = pCs
        posJudulSkr = pJs
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying){
            mediaPlayer.stop()
        }
        if (posLaguSkr<0x7f0c0002){
            posLaguSkr++
            posCoverSkr++
            jud(posLaguSkr)

        }else{
            posLaguSkr = 0x7f0c0001
            posCoverSkr = 0x7f060062
            jud(posLaguSkr)
        }
        audioplaygo(posLaguSkr,posCoverSkr,posJudulSkr)
    }
    fun jud(a:Int){
        if(a==0x7f0c0000){
            idJudul="ledzeppelin"

        }
        else if(a==0x7f0c0001){
            idJudul="oasis"
        }
        else{
            idJudul="queen"
        }
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying){
            mediaPlayer.stop()
        }
        if(posLaguSkr>0x7f0c0001){
            posLaguSkr--
            posCoverSkr--
            jud(posLaguSkr)
        }else{
            posLaguSkr = 0x7f0c0002
            posCoverSkr = 0x7f060063
            jud(posLaguSkr)
        }
        audioplaygo(posLaguSkr,posCoverSkr,posJudulSkr)
    }

    fun audioStop(){
        if (mediaPlayer.isPlaying) {
            mediaPlayer.stop()
        }
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekBar.progress = currTime
            if (currTime != mediaPlayer.duration) handler.postDelayed(this,50)
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay->{
                audioplaygo(idMusik,idCover,idJudul)
            }
            R.id.btnStop->{
                audioStop()
            }
            R.id.btnNext->{
                audioNext()
            }
            R.id.btnPrev->{
                audioPrev()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }
}
